package com.itau.game;

import java.util.List;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

public class GameRPCClient {

	/* no exemplo que fizemos, o Game instancia o objeto Questions quando é iniciado para 
	 * recuperar as questões de forma aleatória.
	 * usando mensageria, o Game será o Client do RPC, que solicitará ao Broker uma lista de
	 * questões (as quais serão enviadas ao Broker pelo RPC Server - Questions
	 */

	// QUAL EH A FILA ONDE SERAO BUSCADAS AS QUESTOES?
	private static String QUEUE_NAME = "d.queue.questions.id";
	private static String QUEUE_NAME_FULL = "d.queue.questions.full";
	// QUAL EH O ENDERECO DO BROKER?
	private static String AMQ_SERVER = "tcp://amq.oramosmarcos.com:61616";

	
	public static void main(String[] args) {
		try {
			
	// ABRIR CONEXAO COM O BROKER
			ActiveMQConnectionFactory abreConexaoBroker = new ActiveMQConnectionFactory(AMQ_SERVER);
			Connection connection = abreConexaoBroker.createConnection();
			connection.start();

	// CRIAR UMA SESSAO
			Session sessaoCriada = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
			
	// REFERENCIAR A FILA QUE QUEREMOS POPULAR
			Queue filaComServer = sessaoCriada.createQueue(QUEUE_NAME);
	
			
	// CRIA UM PRODUTOR - PARA DEPOIS CRIAR A FILA TEMPORARIA POR ONDE CHEGARAO AS POSTAGENS
	// DO RPC SERVER
			MessageProducer produtorSolicitacaoClient = sessaoCriada.createProducer(filaComServer);
	
	// CRIA UMA FILA TEMPORARIA
			Destination filaTemporaria = sessaoCriada.createTemporaryQueue();
				
	// CRIA O CONSUMIDOR DA FILA TEMPORARIA (?)
			MessageConsumer consumidorFilaTemporaria = sessaoCriada.createConsumer(filaTemporaria);
				
	// CONSTROI A MENSAGEM QUE SERA ENVIADA
			String numeroQuestoes = "10";
			String randomQuestoes = "yes";
			int numeroQuestoesInt = 10;
				
			MapMessage mensagemSolicitacao = sessaoCriada.createMapMessage();
			mensagemSolicitacao.setString("quantity", numeroQuestoes);
			mensagemSolicitacao.setString("random", randomQuestoes);
				
	// CONFIGURA OS PARAMETROS DA MENSAGEM DE RESPOSTA
			mensagemSolicitacao.setJMSCorrelationID("teste Millena" + numeroQuestoes);
			mensagemSolicitacao.setJMSReplyTo(filaTemporaria);				
				
	// ENVIA A MENSAGEM - A SOLICITACAO AO RPC SERVER
			produtorSolicitacaoClient.send(mensagemSolicitacao);
			System.out.println("Enviado: " + mensagemSolicitacao.getJMSMessageID());
	
	// RECEBE A RESPOSTA DO RPC SERVER - NA FILA TEMPORARIA
			MapMessage respostaRPCServer = (MapMessage) consumidorFilaTemporaria.receive();
				
			// AQUI RECEBEREMOS UMA LISTA DE IDS ENVIADOS PELO SERVER, E TEREMOS QUE PEGAR CADA 
			// UM DOS OBJETOS PARA PARA MOSTRAR NO CONSOLE
				
			List<Integer> msgRetorno = (List<Integer>) respostaRPCServer.getObject("questions"); //?

			for (int i = 1; i < (numeroQuestoesInt); i++) {
				
				// A CONEXAO COM O BROKER CONTINUA ABERTA, BEM COMO A SESSAO
				
				// REFERENCIAR A FILA NOVA
				Queue filaComServerFull = sessaoCriada.createQueue(QUEUE_NAME_FULL);
				
				// CRIAR NOVO PRODUTOR
				MessageProducer produtorSolicitacaoClientFull = sessaoCriada.createProducer(filaComServerFull);
				
				// CRIAR NOVA FILA TEMPORARIA
				Destination filaTemporariaFull = sessaoCriada.createTemporaryQueue();
				
				// CRIA NOVO CONSUMIDOR DA FILA TEMPORARIA
				MessageConsumer consumidorFilaTemporariaFull = sessaoCriada.createConsumer(filaTemporariaFull);			
				
				// CONSTROI A MENSAGEM ENVIADA - PEDINDO AS INFOS DAS PERGUNTAS
				MapMessage mensagemSolicitacaoFull = sessaoCriada.createMapMessage();
				mensagemSolicitacaoFull.setInt("id", msgRetorno.get(i));
				
				mensagemSolicitacaoFull.setJMSCorrelationID("teste Millena Full" + msgRetorno.get(i));
				mensagemSolicitacaoFull.setJMSReplyTo(filaTemporariaFull);
				
				produtorSolicitacaoClientFull.send(mensagemSolicitacaoFull);
				System.out.println("Enviada solicitacao question full: " + mensagemSolicitacaoFull.getJMSMessageID());
				
				System.out.println("|===============================|");
				System.out.println("| Pergunta " + i + ": " + msgRetorno.get(i));
				System.out.println("|===============================|");
				
				MapMessage respostaRPCServerFull = (MapMessage) consumidorFilaTemporariaFull.receive();
				
				System.out.println("| Pergunta " + i + ": " + respostaRPCServerFull.getString("title"));
				System.out.println("| Categoria: " + respostaRPCServerFull.getString("category"));
				System.out.println("| a) " + respostaRPCServerFull.getString("option1"));
				System.out.println("| b) " + respostaRPCServerFull.getString("option2"));
				System.out.println("| c) " + respostaRPCServerFull.getString("option3"));
				System.out.println("| d) " + respostaRPCServerFull.getString("option4"));
			}

			// FECHA A FILA TEMPORARIA - POR ONDE AS MENSAGENS TRAFEGARAM
			consumidorFilaTemporaria.close();
				
			Thread.sleep(100);
				
		} catch (JMSException | InterruptedException ex) {
			ex.printStackTrace();
			
		}
	}	
}

