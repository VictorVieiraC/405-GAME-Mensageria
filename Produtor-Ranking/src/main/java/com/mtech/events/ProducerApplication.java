package com.mtech.events;

import java.util.Random;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ProducerApplication {

	public static void main(String[] args) {
		try {
			// Abrir conexão com o broker
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://amq.oramosmarcos.com:61616");
        	Connection connection = connectionFactory.createConnection();
        	connection.start();
        	
        	// Criar uma sessão
        	Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
        	
        	// A fila que queremos popular
        	Queue queue = session.createQueue("f.queue.ranking"); 
        	
        	// Nosso produtor de eventos
        	MessageProducer producer = session.createProducer(queue);
        	
    		MapMessage msg = session.createMapMessage();
    		msg.setString("playerName", "Bonnato");
    		msg.setString("gameId", "121");
    		msg.setString("hits", "11");
    		msg.setString("misses", "0");
    		msg.setString("total", "15");
    		
    		producer.send(msg);
    		Thread.sleep(100);
    		System.out.println("Sent: " + msg.getJMSMessageID());
    	
		
		} catch(JMSException | InterruptedException ex) {
			ex.printStackTrace();
		}        
	}

}
