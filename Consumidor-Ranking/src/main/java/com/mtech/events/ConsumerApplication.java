package com.mtech.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ConsumerApplication {
	
	
	private static String QUEUE_NAME = "d.queue.ranking.id";
	private static String AMQ_SERVER = "tcp://amq.oramosmarcos.com:61616";

	public static void main(String[] args) {
		
		try {
			
			ActiveMQConnectionFactory abreConexaoBroker = new ActiveMQConnectionFactory(AMQ_SERVER);
			Connection connection = abreConexaoBroker.createConnection();
			connection.start();

			Session sessaoCriada = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
			
			Queue filaComServer = sessaoCriada.createQueue(QUEUE_NAME);
	
			MessageProducer produtorSolicitacaoClient = sessaoCriada.createProducer(filaComServer);
	
			Destination filaTemporaria = sessaoCriada.createTemporaryQueue();
				
			MessageConsumer consumidorFilaTemporaria = sessaoCriada.createConsumer(filaTemporaria);
				
			MapMessage mensagemSolicitacao = sessaoCriada.createMapMessage();
				
			mensagemSolicitacao.setJMSCorrelationID("teste MillenaASDAIOAJSDOIAS");
			mensagemSolicitacao.setJMSReplyTo(filaTemporaria);				
				
			produtorSolicitacaoClient.send(mensagemSolicitacao);
			System.out.println("Enviado: " + mensagemSolicitacao.getJMSMessageID());
	
			MapMessage respostaRPCServer = (MapMessage) consumidorFilaTemporaria.receive();
			

			respostaRPCServer.acknowledge();
			consumidorFilaTemporaria.close();
					
		} catch (JMSException ex) {
			ex.printStackTrace();
			
		}

	}

}
