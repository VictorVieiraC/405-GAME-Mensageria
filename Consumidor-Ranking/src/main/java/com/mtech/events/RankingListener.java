package com.mtech.events;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

public class RankingListener implements MessageListener {

	public void onMessage(Message message) {
		MapMessage msg = (MapMessage) message;
		try {
			System.out.println("-----");
			System.out.println("player: " + msg.getString("playerName"));
			System.out.println("game:   " + msg.getString("gameId"));
			System.out.println("hits:   " + msg.getInt("hits"));
			System.out.println("misses:   " + msg.getInt("misses"));
			System.out.println("total:  " + msg.getInt("total"));
			message.acknowledge();
		} catch (JMSException ex) {
			ex.printStackTrace();
		}
	}

}
