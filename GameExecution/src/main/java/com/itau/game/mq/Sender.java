package com.itau.game.mq;

import javax.jms.Destination;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import java.util.HashMap;
import java.util.List;

import javax.jms.Connection;

public class Sender {
	// QUAL EH A FILA ONDE SERAO BUSCADAS AS QUESTOES?
	public final static String QUEUE_QUESTION_RANDOM = "d.queue.questions.id";
	public final static String QUEUE_GAME_ID = "d.queue.game.id";
	public  final static String QUEUE_QUESTION_FULL = "d.queue.questions.full";
	 
	private MessageProducer produtorSolicitacaoClient;
	
	private Destination filaTemporaria;
	
	private Queue filaComServer ;
	
	private Session sessaoCriada ;
	
	private 	MessageConsumer consumidorFilaTemporaria ;
	
	public final String randomQuestoesYes = "yes";
	public final String randomQuestoesNo = "no";
	
	private Connection connection;
	
	
	public Sender(Connection criaConexaoBroker, String QUEUE) {
		try {
			
				this.connection = criaConexaoBroker;
			// CRIAR UMA SESSAO
					sessaoCriada = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
			
			// REFERENCIAR A FILA QUE QUEREMOS POPULAR
					filaComServer = sessaoCriada.createQueue(QUEUE);
				
			// CRIA UM PRODUTOR - PARA DEPOIS CRIAR A FILA TEMPORARIA POR ONDE CHEGARAO AS POSTAGENS
			// DO RPC SERVER
				    produtorSolicitacaoClient = sessaoCriada.createProducer(filaComServer);
			
			// CRIA UMA FILA TEMPORARIA
					filaTemporaria = sessaoCriada.createTemporaryQueue();
						
			// CRIA O CONSUMIDOR DA FILA TEMPORARIA (?)
					consumidorFilaTemporaria = sessaoCriada.createConsumer(filaTemporaria);
					
					
					
					
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
	}


	public  Object sGame(int idGame){
		
		try {

			MapMessage mensagemSolicitacao = sessaoCriada.createMapMessage();
			mensagemSolicitacao.setString("idGame", String.valueOf(idGame));

			// CONFIGURA OS PARAMETROS DA MENSAGEM DE RESPOSTA
			mensagemSolicitacao.setJMSCorrelationID("GameExecution");
			mensagemSolicitacao.setJMSReplyTo(filaTemporaria);				
					
			// ENVIA A MENSAGEM - A SOLICITACAO AO RPC SERVER
			produtorSolicitacaoClient.send(mensagemSolicitacao);
			System.out.println("Enviado: " + mensagemSolicitacao.getJMSMessageID());
		
			// RECEBE A RESPOSTA DO RPC SERVER - NA FILA TEMPORARIA
			MapMessage respostaRPCServer = (MapMessage) consumidorFilaTemporaria.receive();
				
			// AQUI RECEBEREMOS UMA LISTA DE IDS ENVIADOS PELO SERVER, E TEREMOS QUE PEGAR CADA 
			// UM DOS OBJETOS PARA PARA MOSTRAR NO CONSOLE
				
			//this.connection.close();
			
			return respostaRPCServer.getObject("game");
		} catch (Exception e) {
			return null;
		}
		
		
	}


	public Object sQuestion(int idQuestion) {
		
		try {

			MapMessage mensagemSolicitacao = sessaoCriada.createMapMessage();
			mensagemSolicitacao.setString("id", String.valueOf(idQuestion));

			// CONFIGURA OS PARAMETROS DA MENSAGEM DE RESPOSTA
			mensagemSolicitacao.setJMSCorrelationID("GameExecution");
			mensagemSolicitacao.setJMSReplyTo(filaTemporaria);				
					
			// ENVIA A MENSAGEM - A SOLICITACAO AO RPC SERVER
			produtorSolicitacaoClient.send(mensagemSolicitacao);
			System.out.println("Enviado: " + mensagemSolicitacao.getJMSMessageID());
		
			// RECEBE A RESPOSTA DO RPC SERVER - NA FILA TEMPORARIA
			MapMessage respostaRPCServer = (MapMessage) consumidorFilaTemporaria.receive();
				
			// AQUI RECEBEREMOS UMA LISTA DE IDS ENVIADOS PELO SERVER, E TEREMOS QUE PEGAR CADA 
			// UM DOS OBJETOS PARA PARA MOSTRAR NO CONSOLE
				
			//this.connection.close();
			
			return respostaRPCServer.getObject("answer");
		} catch (Exception e) {
			return null;
		}
	}
	
	
}
