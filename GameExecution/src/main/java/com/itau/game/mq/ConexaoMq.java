package com.itau.game.mq;

import javax.jms.Connection;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

public abstract class ConexaoMq {
	// QUAL EH O ENDERECO DO BROKER?
	private static String AMQ_SERVER = "tcp://amq.oramosmarcos.com:61616";
	
	public static Connection CriaConexaoBroker() {
		
		try {
		
			// ABRIR CONEXAO COM O BROKER
			ActiveMQConnectionFactory abreConexaoBroker = new ActiveMQConnectionFactory(AMQ_SERVER);
			Connection connection = abreConexaoBroker.createConnection();
			connection.start();

		
		return connection;
		
		} catch (Exception e) {
			return null;
		}
		
		
		
	}
	
}
