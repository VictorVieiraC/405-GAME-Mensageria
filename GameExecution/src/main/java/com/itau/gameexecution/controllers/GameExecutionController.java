package com.itau.gameexecution.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itau.game.mq.ConexaoMq;
import com.itau.game.mq.Sender;
import com.itau.gameexecution.models.GameExecution;
import com.itau.gameexecution.models.Question;
import com.itau.gameexecution.models.QuestionResposta;
import com.itau.gameexecution.models.QuestionRespostaId;
import com.itau.gameexecution.repository.GameExecutionRepository;
import com.itau.gameexecution.repository.QuestionRepository;
import com.itau.gameexecution.repository.QuestionRespostaRepository;



@RestController
@CrossOrigin
@RequestMapping("/gameexecution")
public class GameExecutionController {
	
	
	
	static RestTemplate restTemplate = new RestTemplate();
	
	@Autowired
	GameExecutionRepository gameExecutionRepository;
	
	
	@Autowired
	QuestionRespostaRepository questionRespostaRepository;

	@Autowired
	QuestionRepository questionRespository;
	
	// Um gameExecution especifico
	@RequestMapping(method=RequestMethod.GET, path="/state/{id}")
	public ResponseEntity<GameExecution> getState(@PathVariable int id) {
		Optional<GameExecution> optionalGame= gameExecutionRepository.findById(id);
		if (!optionalGame.isPresent()) {
			return ResponseEntity.status(404).build();
		}
		return ResponseEntity.ok().body(optionalGame.get());
	}
	
	
	//Jogar
	@RequestMapping(method=RequestMethod.POST, path="/play")
	public ResponseEntity<GameExecution> inserirGame(@Valid @RequestBody GameExecution gameExecution) {
	
		try {
			ArrayList<Question> questions = new ArrayList();
			
			questions = addQuestions(getGame(gameExecution.getIdGame()));
			
			gameExecution.setQuestions(questions);
			
			gameExecutionRepository.save(gameExecution);
			
			return ResponseEntity.ok().body(gameExecution);
			
		} catch (Exception e) {

			return ResponseEntity.badRequest().build();
		}
		
	}
	
	//Jogar
		@RequestMapping(method=RequestMethod.POST, path="/playmq")
		public ResponseEntity<GameExecution> playGameMq(@Valid @RequestBody GameExecution gameExecution) {
		
			try {
				ArrayList<Question> questions = new ArrayList();
				
				questions = addQuestions(getGameMq(gameExecution.getIdGame()));
				
				gameExecution.setQuestions(questions);
				
				gameExecutionRepository.save(gameExecution);
				
				return ResponseEntity.ok().body(gameExecution);
				
			} catch (Exception e) {

				return ResponseEntity.badRequest().build();
			}
			
		}
		
	
	//Responder
	@RequestMapping(method=RequestMethod.PUT, path="/responder")
	public ResponseEntity<QuestionResposta> responderPergunta(@Valid @RequestBody QuestionResposta questionResposta) {
	
		try {
			
			

			questionRespostaRepository.save(questionResposta);
			
			return ResponseEntity.ok().body(questionResposta);
			
		} catch (Exception e) {

			return ResponseEntity.badRequest().build();
		}
		
	}
	
	
	//Responder
	@RequestMapping(method=RequestMethod.PUT, path="/respondermq")
	public ResponseEntity<QuestionResposta> responderPerguntamq(@Valid @RequestBody QuestionResposta questionResposta) {
	
		try {
			questionRespostaRepository.save(questionResposta);
			
			Optional<GameExecution> game = gameExecutionRepository.findByid(questionResposta.getId().getIdGameExecution());
			
			if(!game.isPresent()) {
				
				return ResponseEntity.status(404).build();
			}
			
			game.get().setCurrentQuestion(game.get().getCurrentQuestion() + 1);
			
			game.get().setNextQuestion(game.get().getQuestionPointer(game.get().getCurrentQuestion() + 1).getId());
			
	
			return ResponseEntity.ok().body(questionResposta);
			
		} catch (Exception e) {

			return ResponseEntity.badRequest().build();
		}
		
	}
	

	//Finalizar
	@RequestMapping(method=RequestMethod.POST, path="/end")
	public ResponseEntity<GameExecution> finalizarJogo(@Valid @RequestBody GameExecution gameExecution) {
	
		try {

			gameExecutionRepository.save(gameExecution);
			
			return ResponseEntity.ok().body(gameExecution);
			
		} catch (Exception e) {

			return ResponseEntity.badRequest().build();
		}
		
	}
	
	//Finalizar
		@RequestMapping(method=RequestMethod.POST, path="/endmq")
		public ResponseEntity<GameExecution> finalizarJogomq(@Valid @RequestBody GameExecution gameExecution) {
		
			try {
				int Pontos = 0;
				Optional<List<QuestionResposta>> lstquestionResposta = questionRespostaRepository.findByIdGame(gameExecution.getId());
				
				if (lstquestionResposta.isPresent()) {
		
					for(QuestionResposta question : lstquestionResposta.get()) {

						Sender sender = new Sender(ConexaoMq.CriaConexaoBroker(), Sender.QUEUE_QUESTION_FULL );
						
						//HashMap<String, String> questionOriginal =  (HashMap<String, String>) sender.sQuestion(question.getId().getIdQuestion());
						
						if (Integer.parseInt(String.valueOf(sender.sQuestion(question.getId().getIdQuestion()))) == question.getResposta()) {
							
							Pontos ++;	
						}
					}
					
					gameExecution.setPontuacao(Pontos);
					
					gameExecution.setAtivo(false);
					
					gameExecutionRepository.save(gameExecution);
					
					return ResponseEntity.ok().body(gameExecution);
				}else {
					
					return ResponseEntity.status(404).build();
					
				}
				
				
			} catch (Exception e) {

				return ResponseEntity.badRequest().build();
			}
			
		}
	
	
	public ArrayList<Question> addQuestions(List<Integer> ids)
	{
		
		try {
			
			List<Integer> idsSalvos = ids;
			
			List<Question> questionsBanco = new ArrayList();
			
			//questionsBanco = (List<Question>) questionRepository.findAllById(ids);
			
			//ids.removeAll(questionsBanco);
				
			for(int idQuestion : ids)
			{
	
				Question question = new Question();
					
					question.setId(idQuestion);
				
					questionRespository.save(question);
			} 
			
			return (ArrayList<Question>) questionRespository.findAllById(idsSalvos);
			
		} catch (Exception e) {
			throw e;
		}
		
	}
	
	public List<Integer> getGame(int id){
		
		
		try {
			
			String url = "http://localhost:8000/game/" + id;

			ResponseEntity<GameExecution> response = restTemplate.exchange(url, HttpMethod.GET, null , new ParameterizedTypeReference<GameExecution>(){});
			
			GameExecution game = response.getBody();

		
			List<Integer> idQuestions = new ArrayList();
			
			for(Question question : game.getQuestions()) {
				
				idQuestions.add(question.getId());
					
			}
			
			return idQuestions;
		
		} catch (Exception e) {
			throw e;
		}
		
		
	}
	

	public List<Integer> getGameMq(int id){
		
		
		try {
			
			Sender sender = new Sender(ConexaoMq.CriaConexaoBroker(), Sender.QUEUE_GAME_ID );
			
			
			HashMap<String, Object> game =  (HashMap<String, Object>) sender.sGame(id);
			
			List<Integer> idQuestions = new ArrayList<Integer>();
			
			
			for(Map.Entry<String,Object> question :  ((HashMap<String, Object>) game.get("questions")).entrySet())  
			{
				
				idQuestions.add(Integer.valueOf(question.getValue().toString()));
		
			}
			
			return idQuestions;
		
		} catch (Exception e) {
			throw e;
		}
		
		
	}
	
	
}
