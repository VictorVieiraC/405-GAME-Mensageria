package com.itau.gameexecution.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.gameexecution.models.GameExecution;

public interface GameExecutionRepository extends CrudRepository<GameExecution, Integer>{
	public Optional<GameExecution> findByid(int id);
}
