package com.itau.gameexecution.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.itau.gameexecution.models.QuestionResposta;
import com.itau.gameexecution.models.QuestionRespostaId;


public interface QuestionRespostaRepository extends CrudRepository<QuestionResposta, QuestionRespostaId>{
	public Optional<QuestionResposta> findByid(int id);
	
	@Query(value="SELECT u FROM QuestionResposta u WHERE u.id.idGameExecution = :idGameExecution")
	public Optional<List<QuestionResposta>> findByIdGame(@Param("idGameExecution") int idGameExecution);
}
