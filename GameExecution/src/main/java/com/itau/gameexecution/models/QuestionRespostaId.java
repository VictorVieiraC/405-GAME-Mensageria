package com.itau.gameexecution.models;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class QuestionRespostaId implements Serializable {
	private int idQuestion;
	private int idGameExecution;
     
	// must have a default constructor
	public QuestionRespostaId() { }
     
	public QuestionRespostaId(int idQuestion, int idGameExecution) {
		this.idQuestion = idQuestion;
		this.idGameExecution = idGameExecution;
	}

	public int getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}

	public int getIdGameExecution() {
		return idGameExecution;
	}

	public void setIdGameExecution(int idGameExecution) {
		this.idGameExecution = idGameExecution;
	}
     
	// getter e setter + equal e hasCode
}