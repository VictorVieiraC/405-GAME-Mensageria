package com.itau.game;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.itau.game.mq.Sender;
import com.itau.game.repositories.GameRepository;
import com.itau.game.repositories.QuestionRepository;

@Component
public class ConfigGameSender {

	@Autowired
	GameRepository gameRepository;
	
	@Autowired
	QuestionRepository questionRepository;
	
	@Bean
	private boolean consomeFilaSender() {

		try {
	    	ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://amq.oramosmarcos.com:61616");
	    	Connection connection = connectionFactory.createConnection();
	    	connection.start();
	    	
	    	// Criar uma sessão
	    	Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
	    	
	    	// A fila que queremos popular
	    	Queue queue = session.createQueue("d.queue.game.id");
	    	
	    	// Criar um consumidor dessa fila
	    	MessageConsumer consumer = session.createConsumer(queue);
	    	consumer.setMessageListener(new Sender(session, gameRepository, questionRepository));      
	    	
	    	System.out.println("Waiting messages...");
	    	
			} catch(JMSException ex) {
				ex.printStackTrace();
			}
		
			return true;
		}
	
}
