package com.itau.game.mq;

import java.util.Optional;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.springframework.http.ResponseEntity;

import com.itau.game.models.Game;
import com.itau.game.repositories.GameRepository;




public class Receiver implements MessageListener{

	GameRepository gameRepository;
	
	public Receiver(GameRepository gameRepository) {
		this.gameRepository = gameRepository;
	}
	
	public void onMessage(Message message) {
	
	}
}
