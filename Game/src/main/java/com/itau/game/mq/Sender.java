package com.itau.game.mq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.game.models.Game;
import com.itau.game.models.Question;
import com.itau.game.repositories.GameRepository;
import com.itau.game.repositories.QuestionRepository;

public class Sender implements MessageListener {

//	private String QUEUE_NAME = "d.queue.game.id";
//	private String AMQ_SERVER = "tcp://amq.oramosmarcos.com:61616";

	GameRepository gameRepository;
	
	QuestionRepository questionRepository;

	Session session;

	public Sender(Session session, GameRepository gameRepository, QuestionRepository questionRepository) {
		this.session = session;
		this.gameRepository = gameRepository;
		this.questionRepository = questionRepository;
	}

	public void onMessage(Message request) {
		MapMessage msg = (MapMessage) request;
		try {
			System.out.println("Recebi um pedido!" + msg.getString("idGame").toString());
			
			String idGame = msg.getString("idGame").toString();

			Optional<Game> optionalGame= gameRepository.findById(Integer.parseInt(idGame));
			
			MapMessage response = session.createMapMessage();
			
			response.setJMSCorrelationID(request.getJMSCorrelationID());
			
			HashMap<String, Object> body = new HashMap<String, Object>();
			
			
			if (optionalGame.isPresent()) {

				Game game = optionalGame.get();
				
				body.put("idGame", String.valueOf(game.getIdGame()));
				body.put("name",game.getName());
				body.put("numberOfQuestions", String.valueOf(game.getNumberOfQuestions()));
				
				HashMap<String, String> bodyQuestion = new HashMap<String, String>();

				int cont = 0; 
				
				List<Question> ob = game.getQuestions();
				
				for(Question question : ob) {
					
					bodyQuestion.put("idQuestion" + cont, String.valueOf(question.getId()));
					cont ++;
				}
				
				body.put("questions", bodyQuestion);
				
			}			

		
			
			response.setObject("game", body);
			
			MessageProducer producer = session.createProducer(request.getJMSReplyTo());
			producer.send(response);

			request.acknowledge();
		

		} catch (JMSException ex) {
			ex.printStackTrace();
		}
	}

}
