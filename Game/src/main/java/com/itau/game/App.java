package com.itau.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;


import com.itau.game.mq.Receiver;
import com.itau.game.repositories.GameRepository;



@SpringBootApplication
public class App 
{


    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
        
     
    }
}
