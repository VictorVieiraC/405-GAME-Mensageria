package com.itau.game.controllers;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.From;
import javax.validation.Valid;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.game.models.Game;
import com.itau.game.models.Question;
import com.itau.game.repositories.GameRepository;
import com.itau.game.repositories.QuestionRepository;
import com.itau.game.mq.*;

@RestController
@CrossOrigin
@RequestMapping("/game")
public class GameController {

	static RestTemplate restTemplate = new RestTemplate();
    private ObjectMapper MAPPER = new ObjectMapper();
    
	@Autowired
	GameRepository gameRepository;
	
	@Autowired
	QuestionRepository questionRepository;

	// Um professor especifico
	@RequestMapping(method=RequestMethod.GET, path="/{id}")
	public ResponseEntity<Game> getGame(@PathVariable int id) {
		Optional<Game> optionalGame= gameRepository.findById(id);
		if (!optionalGame.isPresent()) {
			return ResponseEntity.status(404).build();
		}

		
		return ResponseEntity.ok().body(optionalGame.get());
	}
		



	//	 Todos os Games
	@RequestMapping(method=RequestMethod.GET, path="/all")
	public ResponseEntity<Iterable<Game>> getGames() {

		return ResponseEntity.ok().body(gameRepository.findAll());
	}
	
//	Novo Game
	@RequestMapping(method=RequestMethod.POST, path="/new")
	public ResponseEntity<Game> inserirGame(@Valid @RequestBody Game game) {
	
		try {
			
			ArrayList<Question> questions = new ArrayList();
			
			questions = addQuestions(buscaQuestions(game.getNumberOfQuestions()));
			
			game.setQuestions(questions);
			
			gameRepository.save(game);
			
			return ResponseEntity.ok().body(game);
			
		} catch (Exception e) {

			return ResponseEntity.badRequest().build();
		}
		
	}
	
//	Novo Game - Mq
//	@RequestMapping(method=RequestMethod.POST, path="/newMq")
//	public ResponseEntity<Game> inserirGameMq(@Valid @RequestBody Game game) {
//	
//		try {
//			
//			ArrayList<Question> questions = new ArrayList();
//			
//			Sender sender = new Sender(ConexaoMq.CriaConexaoBroker());
//			
//			questions = addQuestions(sender.sQuestionRandom(10, sender.randomQuestoesYes)); 
//	
//			game.setQuestions(questions);
//			
///			gameRepository.save(game);
			
//
///			return ResponseEntity.ok().body(game);
	//		
//	} catch (Exception e) {
//
//			return ResponseEntity.badRequest().build();
//		}
//
//		
//	}
	
	public ArrayList<Question> addQuestions(List<Integer> ids)
	{
		
		try {
			
			List<Integer> idsSalvos = ids;
			
			List<Question> questionsBanco = new ArrayList();
			
			//questionsBanco = (List<Question>) questionRepository.findAllById(ids);
			
			//ids.removeAll(questionsBanco);
				
			for(int idQuestion : ids)
			{
	
					Question question = new Question();
					
					question.setId(idQuestion);
				
					questionRepository.save(question);
			} 
			
			return (ArrayList<Question>) questionRepository.findAllById(idsSalvos);
			
		} catch (Exception e) {
			throw e;
		}
		
	}
	
	public List<Integer> buscaQuestions(int quantidade){
		
		
		try {
			
		

			String url = "http://10.162.110.210:8080/question/random/" + quantidade;
			
			//String response = restTemplate.getForObject(url, String.class);
			
			
			ResponseEntity<List<Question>> response = restTemplate.exchange(url, HttpMethod.GET, null , new ParameterizedTypeReference<List<Question>>(){});
			
			ArrayList<Question> questions = (ArrayList<Question>) response.getBody();
	
			List<Integer> idQuestions = new ArrayList();
			
			for(Question question : questions) {
				
				idQuestions.add(question.getId());
					
			}
			
			return idQuestions;
		
		} catch (Exception e) {
			throw e;
		}
		
		
	}
		
	
}
