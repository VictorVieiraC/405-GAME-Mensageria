package com.itau.questionsmicroservices;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import com.itau.questionsmicroservices.models.Question;
import com.itau.questionsmicroservices.repository.QuestionRepository;

public class RequestListenerID implements MessageListener {

	Session session;
	QuestionRepository questionRepository;

	public RequestListenerID(Session session, QuestionRepository questionRepository) {
		this.session = session;
		this.questionRepository = questionRepository;
	}

	public void onMessage(Message request) {

		MapMessage requestMap = (MapMessage) request;
		String quantity, random;
		try {
			System.out.println("NOVO PEDIDO DE QUESTOES");
			quantity = requestMap.getString("quantity");
			random   = requestMap.getString("random");

			System.out.println("NUMERO DE QUESTOES: " + quantity + "| RANDOM: " + random);
			
			Iterable<Question> listQuestion = questionRepository.findAll();
			List<Question> questions = new ArrayList<Question>();
			List<Integer> questionsExit = new ArrayList<Integer>();		
			
			for (Question question : listQuestion) {
				questions.add(question);
			}
			
			int posicao = 0;
			
			for (int i=0;i< Integer.parseInt(quantity);i++) {
			
				if (random.equals("yes")) {
					posicao = (int)Math.ceil(Math.random()*(questions.size()-1));
					questionsExit.add(questions.get(posicao).getId());
					questions.remove(posicao);
				}
				else { 
					posicao = i;
					questionsExit.add(questions.get(posicao).getId());
				}
			
			}

			MapMessage response = session.createMapMessage();
			response.setJMSCorrelationID(request.getJMSCorrelationID());
			
			response.setInt("status", 200);
			response.setObject("questions", questionsExit);
			
			MessageProducer producer = session.createProducer(request.getJMSReplyTo());
			producer.send(response);

			request.acknowledge();
			
			producer.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
}
