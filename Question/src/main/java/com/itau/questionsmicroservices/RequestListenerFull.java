package com.itau.questionsmicroservices;

import java.util.Optional;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;

import com.itau.questionsmicroservices.models.Question;
import com.itau.questionsmicroservices.repository.QuestionRepository;

public class RequestListenerFull implements MessageListener {

	Session session;
	QuestionRepository questionRepository;

	public RequestListenerFull(Session session,  QuestionRepository questionRepository) {
		this.session = session;
		this.questionRepository = questionRepository;
	}

	public void onMessage(Message request) {
		MapMessage requestMap = (MapMessage) request;
		int id;
		try {
			System.out.println("NOVO PEDIDO DE PERGUNTA");
			id = requestMap.getInt("id");
			System.out.println("ID PEDIDO: " + id);

			MapMessage response = session.createMapMessage();
			response.setJMSCorrelationID(request.getJMSCorrelationID());
			
			Optional<Question> question = questionRepository.findByid(id);
			
			if (question.isPresent()) {
				response.setInt("status", 200);
				response.setString("title", question.get().getTitle());
				response.setString("category", question.get().getCategory());
				response.setString("option1", question.get().getOption1());
				response.setString("option2", question.get().getOption2());
				response.setString("option3", question.get().getOption3());
				response.setString("option4", question.get().getOption4());
				response.setInt("answer", question.get().getAnswer());
			}			

			MessageProducer producer = session.createProducer(request.getJMSReplyTo());
			producer.send(response);

			request.acknowledge();
			

			producer.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
}
