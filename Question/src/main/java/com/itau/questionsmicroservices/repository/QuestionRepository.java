package com.itau.questionsmicroservices.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.questionsmicroservices.models.Question;

public interface QuestionRepository extends CrudRepository<Question, Integer>{
	public Optional<Question> findByid(int id);
}
