package com.itau.questionsmicroservices;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.itau.questionsmicroservices.repository.QuestionRepository;

@Component
public class RPCServerQuestionID {

	private String QUEUE_NAME = "d.queue.questions.id";
	private String AMQ_SERVER = "tcp://amq.oramosmarcos.com:61616";
	
	@Autowired
	QuestionRepository questionRepository;

	@Bean
	public boolean ResponderMarieta() {
		
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(AMQ_SERVER);
			Connection connection = connectionFactory.createConnection();
			connection.start();
			
        	// Criar uma sessão
        	Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
        	
        	// A fila que queremos popular
        	Queue queue = session.createQueue(QUEUE_NAME);
        	
        	MessageConsumer consumer = session.createConsumer(queue);
        	consumer.setMessageListener(new RequestListenerID(session, questionRepository));
						
		} catch (JMSException ex) {
			ex.printStackTrace();
		}
		return true;
	}

}
