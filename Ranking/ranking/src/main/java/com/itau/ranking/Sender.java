package com.itau.ranking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.itau.ranking.models.Ranking;
import com.itau.ranking.repositories.RankingRepository;

public class Sender implements MessageListener {

//	private String QUEUE_NAME = "d.queue.ranking.id";
//	private String AMQ_SERVER = "tcp://amq.oramosmarcos.com:61616";

	RankingRepository rankingRepository;

	Session session;

	public Sender(Session session, RankingRepository rankingRepository) {
		this.session = session;
		this.rankingRepository = rankingRepository;
	}

	public void onMessage(Message request) {

		MapMessage requestMap = (MapMessage) request;

		try {

			Iterable<Ranking> ranking = rankingRepository.findAll();
			List<HashMap<String, String>> objeto = new ArrayList<HashMap<String, String>>();

			for (Ranking ranking2 : ranking) {
				HashMap<String, String> body = new HashMap<String, String>();

				body.put("playerName", ranking2.getPlayerName());
				body.put("gameId", Integer.toString(ranking2.getGameId()));
				body.put("hits", Integer.toString(ranking2.getHits()));
				body.put("misses", Integer.toString(ranking2.getMisses()));
				body.put("total", Integer.toString(ranking2.getTotal()));

				objeto.add(body);
				
				System.out.println("------------ Ranking enviado: " + body + " ------------");
			}
			
			MapMessage response = session.createMapMessage();

			response.setJMSCorrelationID(request.getJMSCorrelationID());

			response.setObject("ranking", objeto);

			MessageProducer producer = session.createProducer(request.getJMSReplyTo());

			producer.send(response);

			producer.close();

			request.acknowledge();

		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

}
