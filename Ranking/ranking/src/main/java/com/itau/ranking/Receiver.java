package com.itau.ranking;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.itau.ranking.models.Ranking;
import com.itau.ranking.repositories.RankingRepository;


public class Receiver implements MessageListener{

	RankingRepository rankingRepository;
	
	public Receiver(RankingRepository rankingRepository) {
		this.rankingRepository = rankingRepository;
	}
	
	public void onMessage(Message message) {
		MapMessage msg = (MapMessage) message;
		try {

			Ranking rank = new Ranking();
			
			rank.setPlayerName(msg.getString("playerName").toString());
			rank.setGameId((int)msg.getLong("gameId"));
			rank.setHits((int)msg.getInt("hits"));
			rank.setMisses((int)msg.getInt("misses"));
			rank.setTotal((int)msg.getInt("total"));

			rankingRepository.save(rank);
			
			System.out.println("------------ Ranking inserido com sucesso: " + rank + " ------------");
			
			message.acknowledge();

		} catch (JMSException ex) {
			ex.printStackTrace();
		}
	}
}
