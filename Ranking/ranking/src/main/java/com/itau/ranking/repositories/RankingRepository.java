package com.itau.ranking.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.itau.ranking.models.Ranking;

public interface RankingRepository extends CrudRepository<Ranking, Long>{

	public Optional<Ranking> findByIdRanking(int idRanking);
	
	@Query(value="SELECT u FROM Ranking u WHERE u.gameId = :gameId ORDER BY u.hits DESC")
	public Optional<List<Ranking>> findByIdGame(@Param("gameId") int gameId);
	
	@Query(value="SELECT u FROM Ranking u ORDER BY u.hits DESC")
	public Iterable<Ranking> findAll();
	
}
